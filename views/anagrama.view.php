<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?php echo $data['titulo']; ?></h1>

</div>

<!-- Content Row -->

<div class="row">    
    <div class="col-12">
        <?php
        if(isset($data['resultado'])){
        ?>
        <div class="alert <?php echo $data['resultado'] ? 'alert-success' : 'alert-danger'; ?>">
            <strong><?php
            echo $data['resultado'] ? 'Son anagrama' : 'No son anagrama';
            ?></strong>
        </div>
        <?php
        }
        ?>
        <div class="card shadow mb-4">
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary"><?php echo $data['div_titulo']; ?></h6>                                    
            </div>
            <!-- Card Body -->
            <div class="card-body">

                <form action="./?sec=anagrama" method="post">
                    <div class="container-fluid">
                        <div class="row">                            
                            <!--<form method="get">-->                            
                            <div class="mb-3 col-lg-6">
                                <label for="palabra1">Palabra 1<span class="text-danger">*</span></label>
                                <input class="form-control" id="palabra1" type="text" name="palabra1" placeholder="Inserte la primera palabra" value="<?php echo isset($data['sanitized']['palabra1']) ? $data['sanitized']['palabra1'] : ""; ?>">                                
                            </div>                                                                                 
                            <div class="mb-3 col-lg-6">
                                <label for="palabra2">Palabra 2<span class="text-danger">*</span></label>
                                <input class="form-control" id="palabra2" type="text" name="palabra2" placeholder="Inserte la segunda palabra" value="<?php echo isset($data['sanitized']['palabra2']) ? $data['sanitized']['palabra2'] : ""; ?>">                                
                            </div>                                                                                 
                            <div class="mb-3 col-lg-12">
                                <input type="submit" value="Enviar" name="submit" class="btn btn-primary"/>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>                        
</div>

