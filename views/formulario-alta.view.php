<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?php echo $data['titulo']; ?></h1>

</div>

<!-- Content Row -->

<div class="row">
<?php
var_dump($data);
var_dump($_POST);
?>
    <div class="col-12">
        <div class="card shadow mb-4">
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary"><?php echo $data['div_titulo']; ?></h6>                                    
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <form action="./?sec=formulario-alta" method="post">
                    <input type="hidden" name="sec" value="formulario-alta" />
                    <div class="row">
                    <div class="mb-3 col-12">
                        <label for="username">Nombre de usuario:</label>
                        <input class="form-control" id="username" type="text" name="username" placeholder="Nombre de usuario" value="<?php echo isset($data[S_POST]['username']) ? $data[S_POST]['username'] : ""; ?>">
                        <?php if(isset($data['errors']['username'])){ ?>
                        <p class="text-danger"><small><?php echo $data['errors']['username']; ?></small></p>
                        <?php } ?>
                    </div>                    
                    <div class="mb-3 col-sm-6">
                        <label for="password1">Contraseña:</label>
                        <input class="form-control" id="password1" type="password" name="password1" placeholder="Contraseña">
                        <?php if(isset($data['errors']['password1'])){ ?>
                        <p class="text-danger"><small><?php echo $data['errors']['password1']; ?></small></p>
                        <?php } ?>
                    </div>
                    <div class="mb-3 col-sm-6">
                        <label for="password2">Repetir Contraseña:</label>
                        <input class="form-control" id="password2" type="password" name="password2" placeholder="Repetir contraseña">
                        <?php if(isset($data['errors']['password2'])){ ?>
                        <p class="text-danger"><small><?php echo $data['errors']['password2']; ?></small></p>
                        <?php } ?>
                    </div>
                    <div class="mb-3">
                        <input type="submit" value="Enviar" name="submit" class="btn btn-primary"/>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>                        
</div>

