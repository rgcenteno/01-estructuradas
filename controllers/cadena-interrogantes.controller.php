<?php
$data = array();
$data['titulo'] = "Cadena interrogantes";
$data["div_titulo"] = "Ejercicios repaso";

//Por comodidad creamos arrays para las variables que se pueden recibir por post y son arrays


//Comprobamos si se ha enviado el formulario y si es así, lo procesamos
if(isset($_POST['submit'])){
    $data['formSent'] = TRUE;    

    $data['sanitized'] = sanitizeInput($_POST);
    
    $data['resultado'] = cumpleRegla($_POST['palabra1']);
   
}

function cumpleRegla(string $test) : bool{
    $primer = -1;
    $segundo = -1;
    $contador = 0;
    for($i = 0; $i < strlen($test); $i++){
        if($test[$i] == '?'){
            $contador++;
        }
        elseif(is_numeric($test[$i])){
            if($primer == -1){
                $primer = (int)$test[$i];
                $contador = 0;
            }
            else{
                $segundo = (int)$test[$i];
                if(($primer+$segundo == 10) && $contador != 3){
                    return false;
                }
                else{
                    $primer = $segundo;
                    $segundo = -1;
                    $contador = 0;
                }
            }
        }
    }
    return true;
}

function sanitizeInput(array $_p): array{    
    $_data = filter_var_array($_p, FILTER_SANITIZE_SPECIAL_CHARS);    
    return $_data;
}

include 'views/templates/header.php';
include 'views/cadena-interrogantes.view.php';
include 'views/templates/footer.php';