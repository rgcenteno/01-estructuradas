<?php
define('S_POST', 'sanitized_post');
define('OPCIONS_VALIDAS', array('a', 'b', 'c'));
define('DEBUG', TRUE);

$data = array();
$data['titulo'] = "Iterativas 01";
$data["div_titulo"] = "Ejercicios arrays";

//Por comodidad creamos arrays para las variables que se pueden recibir por post y son arrays


//Comprobamos si se ha enviado el formulario y si es así, lo procesamos
if(isset($_POST['submit'])){
    $data['formSent'] = TRUE;    
    /*
     * Primero comprobamos los errores y después si no hay errores, se procesaría el formulario. Nombre es un campo obligatorio pero textarea no por eso no lo comprobamos.
     */
    $data['errors'] = checkForm($_POST);
    //echo json_encode($data['errors']);die;
    
    /*
     * Dependiendo del escenario, nos interesa mostrar los datos que insertó el usuario dentro del formulario. Por ejemplo, en un formulario de inserción de datos,
     * si hay un error, no podemos obligar al usuario a meter todos los datos de nuevo si no que debemos mostrar lo que insertó y los errores que hubo. Las variables que insertó
     * debemos "limpiarlas" antes de mostrarlas (por ejemplo quitar etiquetas HTML) ya que un usuario con cierto conocimiento podría desmontarnos nuestra página insertando determinados
     * valores en el código.
     */
    $data[S_POST] = sanitizeInput($_POST);
    
    /**
     * Si no hay errores, ejecutamos la rutina predefinida. Por ejemplo guardar/modificar un registro de datos, generar un documento o realizar cálculos.
     */
    if(count($data['errors']) == 0){
        
    }
    
    
}

function checkForm(array $_p) : array{
    $_errors = array();
    if(strlen($_p['username']) == 0){
        $_errors['username'] = "No ha insertado un nombre de usuario";
    }
    elseif(strlen($_p['username']) <= 2 || strlen($_p['username']) > 15){
        $_errors['username'] = "El nombre de usuario debe contener entre 3 y 15 caracteres";
    }
    elseif(strlen($_p['username']) != strlen(str_replace(" ", "",  $_p['username']))){
        $_errors['username'] = "No se permiten espacios en el nombre de usuario";
    }
    elseif(strlen(filter_var($_p['username'], FILTER_SANITIZE_STRING)) != strlen($_p['username'])){
        $_errors['username'] = "No se permite la insercción de HTML en el nombre de usuario";
        
    }
    elseif(!preg_match("/^[A-Za-z]/", $_p['username'])){
        
        $_errors['username'] = "El nombre de usuario debe empezar por una letra";
    }
    //Falta el primer caracter
    if(strlen($_p['password1']) < 8){
        $_errors['password1'] = "La contraseña debe tener al menos 8 caracteres";
    }
    elseif(filter_var($_p['password1'], FILTER_SANITIZE_STRING) != filter_var($_p['password1'])){
        $_errors['password1'] = "No se permite HTML en la contraseña";
    }
    elseif($_p['password1'] != $_p['password2']){
        $_errors['password2'] = "Las contraseñas no coinciden";
    }
        
    return $_errors;
}

function sanitizeInput(array $_p): array{    
    $_data = filter_var_array($_p, FILTER_SANITIZE_SPECIAL_CHARS);    
    return $_data;
}

include 'views/templates/header.php';
include 'views/formulario-alta.view.php';
include 'views/templates/footer.php';