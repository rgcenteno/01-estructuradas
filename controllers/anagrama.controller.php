<?php
$data = array();
$data['titulo'] = "Anagrama";
$data["div_titulo"] = "Ejercicios repaso";

//Por comodidad creamos arrays para las variables que se pueden recibir por post y son arrays


//Comprobamos si se ha enviado el formulario y si es así, lo procesamos
if(isset($_POST['submit'])){
    $data['formSent'] = TRUE;    

    $data['sanitized'] = sanitizeInput($_POST);
 
    if(count($data['errors']) == 0){
        $data['resultado'] = anagrama($_POST['palabra1'], $_POST['palabra2']);
    }
    
    
}

function anagrama(string $s1, string $s2) : bool{
    $s1 = strtolower($s1);
    $s2 = strtolower($s2);
    $s1 = preg_replace("/[^a-z0-9]/", "", $s1);
    $s2 = preg_replace("/[^a-z0-9]/", "", $s2);
    $a1 = str_split($s1);
    $a2 = str_split($s2);
    sort($a1);
    sort($a2);  
    return $a1 == $a2;
}

function sanitizeInput(array $_p): array{    
    $_data = filter_var_array($_p, FILTER_SANITIZE_SPECIAL_CHARS);    
    return $_data;
}

include 'views/templates/header.php';
include 'views/anagrama.view.php';
include 'views/templates/footer.php';